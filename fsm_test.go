package fsm_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitee.com/konglinglong/fsm"
	"github.com/stretchr/testify/require"
)

var testTime = 0

func fsm_test_default_handler1(state fsm.StateType, evtCtxt *fsm.EventContext) (newEvtCtxt *fsm.EventContext, rv fsm.ReturnVal) {
	return nil, 0
}
func fsm_test_idle_state_handler1(state fsm.StateType, evtCtxt *fsm.EventContext) (newEvtCtxt *fsm.EventContext, rv fsm.ReturnVal) {
	return nil, 0
}

func fsm_test_wait_ack_state_handler1(state fsm.StateType, evtCtxt *fsm.EventContext) (newEvtCtxt *fsm.EventContext, rv fsm.ReturnVal) {
	if testTime%2 == 0 {
		rv = 0
	} else {
		rv = -1
	}
	testTime += 1
	return nil, rv
}

func fsm_test_wait_close_state_handler1(state fsm.StateType, evtCtxt *fsm.EventContext) (newEvtCtxt *fsm.EventContext, rv fsm.ReturnVal) {
	return nil, 0
}

func fsmLogFunc(format string, a ...any) {
	fmt.Printf(format, a...)
}

func smInstJsonMarshalAndUnMarshal(t *testing.T, smInst *fsm.FsmInst) (*fsm.FsmInst, error) {
	jtxt, err := json.Marshal(smInst)
	require.NoError(t, err)
	var newSmInst fsm.FsmInst
	err = json.Unmarshal(jtxt, &newSmInst)
	require.NoError(t, err)
	return &newSmInst, err
}

func TestFSM(t *testing.T) {
	sm := fsm.NewFsm("TestFsm", "STATE_IDLE", map[fsm.StateType]*fsm.FsmState{
		"STATE_IDLE": {
			DefaultHandle: nil,
			EventTable: map[fsm.EventType]*fsm.FsmEvent{
				"EVENT_CONNECT": {
					fsm_test_idle_state_handler1, map[fsm.ReturnVal]fsm.StateType{
						0:  "STATE_WAIT_ACK",
						-1: "STATE_IDLE"}}}},
		"STATE_WAIT_ACK": {
			DefaultHandle: &fsm.FsmEvent{
				fsm_test_default_handler1, map[fsm.ReturnVal]fsm.StateType{
					0: "STATE_IDLE"}},
			EventTable: map[fsm.EventType]*fsm.FsmEvent{
				"EVENT_RCV_ACK": {
					fsm_test_wait_ack_state_handler1, map[fsm.ReturnVal]fsm.StateType{
						0:  "STATE_WAIT_CLOSE",
						-1: "STATE_IDLE"}}}},
		"STATE_WAIT_CLOSE": {
			DefaultHandle: nil,
			EventTable: map[fsm.EventType]*fsm.FsmEvent{
				"EVENT_CLOSE": {
					fsm_test_wait_close_state_handler1, map[fsm.ReturnVal]fsm.StateType{
						0: "STATE_IDLE"}}}},
	})
	fmt.Println("sm: ", sm)
	sm.SetLogFunc(fsmLogFunc)

	smInst := fsm.NewFsmInst(sm, "TestFsmInst")
	fmt.Println("sm: ", smInst)

	smInst, err := smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_CONNECT", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_WAIT_ACK", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_CONNECT", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_IDLE", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_CONNECT", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_WAIT_ACK", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_RCV_ACK", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_WAIT_CLOSE", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_CLOSE", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_IDLE", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_CONNECT", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_WAIT_ACK", string(smInst.CurrState()))

	smInst, err = smInstJsonMarshalAndUnMarshal(t, smInst)
	require.NoError(t, err)

	err = smInst.HandleEvent(&fsm.EventContext{"EVENT_RCV_ACK", nil})
	require.NoError(t, err)
	require.Equal(t, "STATE_IDLE", string(smInst.CurrState()))
}
